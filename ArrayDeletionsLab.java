public class ArrayDeletionsLab {
    public static int[] deleteElementByIndex(int[] arr,int Index){
        int[] newArr = new int[arr.length-1];
        for(int i=0,j=0;i<newArr.length;i++,j++){
            if(j == Index){
                newArr[j] = arr[j+1];
                j++;
            }else if(i > Index){
                newArr[i] = arr[j];
            }else{
                newArr[i] = arr[i];
            }
        }return newArr;
    }

    public static int[] deleteElementByValue(int arr[],int value){
        int[] newArr = new int[arr.length-1];
        for(int i=0,j=0;i<newArr.length;i++,j++){
            if(arr[i] != value){
                newArr[i] = arr[i];
            }else if(arr[i] == value){
                newArr[i] = arr[j+1];
            }
        }return newArr;
    }
    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};
        System.out.print("Original array: [");

        for(int i=0;i<arr.length;i++){
            if(i != arr.length-1){
                System.out.print(arr[i]+",");
            }
            if(i == arr.length-1){
                System.out.print(arr[i]);
            }
            
        }System.out.print("]");

        int Index =2;
        arr = deleteElementByIndex(arr,Index);
        System.out.print("\nArray after deleting element at index 2: [");
        for(int i=0;i<arr.length;i++){
            if(i != arr.length-1){
                System.out.print(arr[i]+",");
            }
            if(i == arr.length-1){
                System.out.print(arr[i]);
            }
            
        }System.out.print("]");

        int value = 4;
        arr = deleteElementByValue(arr,value);
        System.out.print("\nArray after deleting element with value 4: [");
        for(int i=0;i<arr.length;i++){
            if(i != arr.length-1){
                System.out.print(arr[i]+",");
            }
            if(i == arr.length-1){
                System.out.print(arr[i]);
            }
        }
        System.out.print("]");
    }
}